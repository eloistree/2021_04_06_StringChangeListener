using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ChangeSubmission : MonoBehaviour
{
    [SerializeField] float m_timeToSubmit=2;

    [SerializeField] float m_currentTimer=0;

    [SerializeField] UnityEvent m_submit;



    [ContextMenu("Trigger Change")]
    public void ChangeObserved()
    {
        m_currentTimer = m_timeToSubmit;
    }


    [ContextMenu("Cancel")]
    public void Cancel()
    {
        m_currentTimer = 0; ;
    }

    void Update()
    {
        if (m_currentTimer > 0) {
            m_currentTimer -= Time.deltaTime;

            if (m_currentTimer <= 0) {
                Cancel();
                m_submit.Invoke();
            }
        }
        
    }
}
