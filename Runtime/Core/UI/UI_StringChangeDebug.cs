using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_StringChangeDebug : MonoBehaviour
{
    public InputField m_previous;
    public InputField m_current;

    public void Change(IStringChange change)
    {
        if (m_previous != null)
            m_previous.text = change.GetPrevious();
        if (m_current != null)
            m_current.text = change.GetNewest();


    }

}
