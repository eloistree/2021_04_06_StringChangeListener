using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[System.Serializable]
public class ContinusString : IContinusChangeString
{
    public string m_value = "";

    public void Append(string text, out string previous)
    {

        previous = m_value.ToString();
        m_value+=(text);
    }

    public void Append(char character, out string previous)
    {

        previous = m_value.ToString();
        m_value+=(character);
    }

    public string GetCurrent()
    {
        return m_value;
    }

    public void Set(string text, out bool hasChanged, out string previous)
    {
        hasChanged = !StringUtility.IsEqualTo ( text,  m_value, false);
        if (hasChanged)
        {
            previous = m_value;
            m_value = text;
        }
        else previous = "";
    }
}