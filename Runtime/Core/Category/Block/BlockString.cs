using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[System.Serializable]
public class BlockString : IBlockChangeString
{
    public string m_value = "";

    public string GetCurrent()
    {
        return m_value;
    }

    public void Set(string text, out bool hasChanged, out string previous)
    {
        hasChanged = ! StringUtility.IsEqualTo( text,  m_value, false);
        if (hasChanged)
        {
            previous = m_value;
            m_value = text;
        }
        else previous = "";
    }
}

