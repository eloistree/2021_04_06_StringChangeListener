using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class BlockStringMono : MonoBehaviour, IBlockChangeString,IStringeChangeEmitor
{
    [TextArea(0,4)]
    public string currentDebug, previousDebug;
    public BlockStringWithEmittor m_changingString= new BlockStringWithEmittor();

    public void AddStringListener(StringHasChanged listener)
    {
        m_changingString.AddStringListener(listener);
    }

    public void RemoveStringListener(StringHasChanged listener)
    {
        m_changingString.RemoveStringListener(listener);
    }
   
    public string GetCurrent()
    {
        return m_changingString.GetCurrent();
    }

    
    public void Set(string text )
    {
        string previous;
        bool c;
        Set(text,out c, out previous);
    }
    public void Set(string text, out bool changed, out string previous)
    {
        currentDebug = text;
        m_changingString.Set(text,out changed, out previous);
        if(changed)
        previousDebug = previous;
    }
}



