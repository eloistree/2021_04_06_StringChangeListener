using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BlockStringWithEmittor : IBlockChangeString, IStringeChangeEmitor
{
    [SerializeField]
    StringChangeEmitor m_changeEmittor = new StringChangeEmitor();
    BlockString m_currentString = new BlockString();

    public void AddStringListener(StringHasChanged listener)
    {
        m_changeEmittor.AddStringListener(listener);
    }
    public void RemoveStringListener(StringHasChanged listener)
    {
        m_changeEmittor.RemoveStringListener(listener);
    }


    public string GetCurrent()
    {
        return m_currentString.GetCurrent();
    }

    public void Set(string text, out bool changed, out string previous)
    {
        m_currentString.Set(text,out changed, out previous);
        if(changed)
            m_changeEmittor.NotifyAsChanged(previous,text);
    }


}



