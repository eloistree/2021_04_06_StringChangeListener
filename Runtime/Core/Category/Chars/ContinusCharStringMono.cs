using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ContinusCharStringMono : MonoBehaviour, IContinusChangeString, IStringeChangeEmitor
{
    public uint m_sizeOfStorage = 10;
    [TextArea(0, 4)]
    public string currentDebug, previousDebug;
    public ContinusCharStringWithEmittor m_changingString = new ContinusCharStringWithEmittor();

    public void SetMaxSize(uint size) { m_changingString.SetMaxSize(size); }
    public void SetMaxSize(string size) {
        uint i;
        if(uint.TryParse(size, out i))
            m_changingString.SetMaxSize(i); 
    }

    private void Awake()
    {
        SetMaxSize(m_sizeOfStorage);
    }
    public void AddStringListener(StringHasChanged listener)
    {
        m_changingString.AddStringListener(listener);
    }

    public void RemoveStringListener(StringHasChanged listener)
    {
        m_changingString.RemoveStringListener(listener);
    }
    public void Append(string text)
    {
        string previous;
        Append(text, out previous);
    }
    public void Append(string text, out string previous)
    {
        m_changingString.Append(text, out previous);
        previousDebug = previous;
        currentDebug = GetCurrent();
    }
    public void Append(char character)
    {
        string previous;
        Append(character, out previous);
    }
    public void Append(char character, out string previous)
    {
        m_changingString.Append(character, out previous);
        previousDebug = previous;
        currentDebug = GetCurrent();
    }

    public string GetCurrent()
    {
        return m_changingString.GetCurrent();
    }


    public void Set(string text)
    {
        string previous;
        bool c;
        Set(text, out c, out previous);
    }
    public void Set(string text, out bool changed, out string previous)
    {
        currentDebug = text;
        m_changingString.Set(text, out changed, out previous);
        if (changed)
            previousDebug = previous;
    }
}
