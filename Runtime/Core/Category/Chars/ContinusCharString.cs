using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinusCharString : IContinusChangeString
{

    public string m_value = "";
    public uint m_maxSize=10;
    public void SetMaxSize(uint size) { m_maxSize = size; }

    public void Append(string text, out string previous)
    {

        bool hasChanged;
        Set(m_value + text, out hasChanged, out previous);
      
    }

    public void Append(char character, out string previous)
    {
       
        bool hasChanged;
        Set(m_value + character,out hasChanged, out previous);
    }

    public string GetCurrent()
    {
        return m_value;
    }

    public void Set(string text, out bool hasChanged, out string previous)
    {
        if (text.Length > m_maxSize) {
            text = text.Substring(text.Length - (int) m_maxSize);
        }
        hasChanged = !StringUtility.IsEqualTo(text, m_value,false);
        if (hasChanged)
        {
            previous = m_value;
            m_value = text;
        }
        else previous = m_value;
    }
}
