using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ContinusCharStringWithEmittor :  IContinusChangeString, IStringeChangeEmitor
{
    
    [SerializeField]
    StringChangeEmitor m_changeEmittor = new StringChangeEmitor();
    ContinusCharString m_currentString = new ContinusCharString();

    public void SetMaxSize(uint size) { m_currentString.SetMaxSize(size); }
    public void AddStringListener(StringHasChanged listener)
    {
        m_changeEmittor.AddStringListener(listener);
    }
    public void RemoveStringListener(StringHasChanged listener)
    {
        m_changeEmittor.RemoveStringListener(listener);
    }

    public void Append(string text, out string previous)
    {
        if (text == null || text.Length == 0)
        {
            previous = m_currentString.GetCurrent();
            return;
        }
        m_currentString.Append(text, out previous);
        m_changeEmittor.NotifyAsChanged(previous, m_currentString.GetCurrent());

    }

    public void Append(char character, out string previous)
    {
        m_currentString.Append(character, out previous);
        m_changeEmittor.NotifyAsChanged(previous,m_currentString.GetCurrent());
    }

    public string GetCurrent()
    {
        return m_currentString.GetCurrent();
    }

    public void Set(string text, out bool changed, out string previous)
    {
        m_currentString.Set(text, out changed, out previous);
        if (changed)
            m_changeEmittor.NotifyAsChanged(previous,m_currentString.GetCurrent() );
    }

}
