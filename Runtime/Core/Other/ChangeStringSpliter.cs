using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeStringSpliter : MonoBehaviour
{
    public StringEmitEvent m_previous;
    public StringEmitEvent m_current;

    public void Change( IStringChange change)
    {
        m_previous.Invoke(change.GetPrevious());
        m_current.Invoke(change.GetNewest());

    }

}
