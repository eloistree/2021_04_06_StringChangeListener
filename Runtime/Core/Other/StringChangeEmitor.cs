using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StringChangeEmitor : IStringeChangeEmitor
{
    private StringHasChanged m_codeListener;
    [SerializeField] StringHasChangedUnitEvent m_editorListner;


    public void NotifyAsChanged(string previous, string current)
    {
        NotifyAsChanged(new StringChangeBean(previous, current));
    }
    public void NotifyAsChanged(StringChangeAbstract change)
    {
        if (m_codeListener != null)
            m_codeListener(change);
        m_editorListner.Invoke(change);
    }

    public void AddStringListener(StringHasChanged listener)
    {
        m_codeListener += listener;
    }

    public void RemoveStringListener(StringHasChanged listener)
    {
        m_codeListener -= listener;
    }
}