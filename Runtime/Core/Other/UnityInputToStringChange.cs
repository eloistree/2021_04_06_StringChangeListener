using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityInputToStringChange : MonoBehaviour
{
    public StringEmitEvent m_emitted;

    void Update()
    {
        if (Input.inputString.Length > 0) {
            m_emitted.Invoke(Input.inputString);
        }
    }
}
