using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringUtility
{
    
     public static bool IsEqualTo( string a,  string b, bool ignorecase)
    {
        if (a.Length != b.Length)
            return false;
        if (ignorecase)
            return (a.ToLower().IndexOf(b.ToLower()) == 0);
        else
            return (a.IndexOf(b) == 0);

    }

    public static bool IsFinishBy( string textToOverwatch,  string textToFound, bool ignoreCase = true)
    {
        if (ignoreCase)
        {
            textToOverwatch = textToOverwatch.ToLower();
            textToFound = textToFound.ToLower();
        }
        if (textToOverwatch.Length < textToFound.Length)
        {
            return false;
        }
        else if (textToOverwatch.Length == textToFound.Length)
        {
            if (textToOverwatch == textToFound)
                return true;
        }
        else if (textToOverwatch.Length > textToFound.Length)
        {
            for (int i = 0; i < textToFound.Length; i++)
            {
                if (textToFound[textToFound.Length - 1 - i] != textToOverwatch[textToOverwatch.Length - 1 - i])
                {
                    return false;
                }

            }
            return true;
        }
        return false;
    }
}