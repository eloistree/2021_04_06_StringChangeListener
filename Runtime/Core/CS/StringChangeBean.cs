using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringChangeBean : StringChangeAbstract
{
    private string m_previous;
    private string m_current;

    public StringChangeBean(string previous, string current)
    {
        ResetValue(previous, current);
    }
    public void ResetValue(string previous, string current)
    {
        m_previous = previous;
        m_current = current;
    }

    public override string GetNewest()
    {
        return m_current;
    }

    public override string GetPrevious()
    {
        return m_previous;
    }
}