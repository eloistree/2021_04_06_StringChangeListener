using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IBlockChangeString
{
    string GetCurrent();
    void Set(string text, out bool hasChanged, out string previous);
}

public interface IContinusChangeString
{
    string GetCurrent();
    void Set(string text, out bool hasChanged, out string previous);
    void Append(string text, out string previous);
    void Append(char character, out string previous);
}


public interface IStringeChangeEmitor
{
     void AddStringListener(StringHasChanged listener);
     void RemoveStringListener(StringHasChanged listener);

}

public delegate void StringHasChanged(IStringChange change);

[System.Serializable]
public class StringHasChangedUnitEvent: UnityEvent<StringChangeAbstract> {}

public interface IStringChange
{
    string GetNewest();
    string GetPrevious();
}
[System.Serializable]
public abstract class StringChangeAbstract : IStringChange
{
    public abstract string GetNewest();
    public abstract string GetPrevious();
}

public interface ITimedStringChange : IStringChange {

    DateTime GetTimeOfChange();
}
[System.Serializable]
public class StringEmitEvent : UnityEvent<string> { }

